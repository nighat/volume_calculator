<?php
include_once "vendor/autoload.php";

use Pondit\Calculator\VolumeCalculator\Volume;
use Pondit\Calculator\VolumeCalculator\Cube;
use Pondit\Calculator\VolumeCalculator\Cone;
use Pondit\Calculator\VolumeCalculator\Cylinder;


echo "<h1 style='text-align: center'>Volume Calculator</h1>";


$volume = new Volume(2,2,4/3);
echo "Volume :-".$volume->getVolume();
echo "<br/>";


$cube = new Cube(50,1/3);
echo "Cube:-".$cube->cube();
echo "<br/>";


$cone = new Cone(50,30,30);
echo "Cone:-".$cone->cone();
echo "<br/>";


$cylinder = new Cylinder( 3.142,8,12);
echo "Cylinder".$cylinder->cylinder();
