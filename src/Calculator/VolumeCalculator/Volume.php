<?php

namespace Pondit\Calculator\VolumeCalculator;


class Volume
{
   public $pi ;
   public $radius;
   public $four_thirds;


   public function __construct($pi,$radius,$four_thirds )
   {
       $this->pi = $pi;
       $this->radius = $radius;
       $this->four_thirds = $four_thirds;
   }
   public function getVolume(){

       $area = $this->four_thirds * $this->pi * ($this->radius *$this->radius);

       return $area;
   }
}