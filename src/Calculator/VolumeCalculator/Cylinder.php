<?php

namespace Pondit\Calculator\VolumeCalculator;


class Cylinder
{


    public $Pi;
    public $radius;
    public $height;

    public function __construct($pi, $radius, $height)
    {
        $this->Pi = $pi;
        $this->radius = $radius;
        $this->height = $height;

    }

    public function cylinder()
    {
       //Formula area	=	2πr2+2πrh
        $surfaceArea = 2 * $this->Pi * $this->radius * $this->radius + 2 * $this->Pi * $this->radius * $this->height;

        return $surfaceArea;
    }
}