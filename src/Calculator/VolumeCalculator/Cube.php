<?php


namespace Pondit\Calculator\VolumeCalculator;


class Cube
{
    public $x;
    public $one_thirds;
    public function __construct($x,$one_thirds)
    {
        $this->x = $x;
        $this->one_thirds = $one_thirds;

    }

    public function cube(){

        $area = $this->x ** $this->one_thirds;
        return $area;
    }

}