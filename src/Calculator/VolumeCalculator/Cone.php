<?php


namespace Pondit\Calculator\VolumeCalculator;


class Cone
{
    public $slant_height;
    public $radius ;
    public $height;
    public function __construct($slant_height,$radius ,$height)
    {
        $this->slant_height=$slant_height;
        $this->radius=$radius;
        $this->height=$height;

    }
    public function cone(){

        $area =  1/3*$this->slant_height * $this->radius * $this->radius * $this->height;

        return $area;
    }
}